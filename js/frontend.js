$(function ()
{

    $('.owl-carousel-our-team').owlCarousel({
        loop:true,
        margin: 0,
        navText: ['',''],
        nav: true,

        responsive:{
            0:{
                items: 4,
                autoWidth: true
            }
        }

    });

    $('.owl-carousel-our-partner').owlCarousel({
        loop:true,
        margin: 0,
        navText: ['',''],
        nav: true,

        responsive:{
            0:{
                items: 4,
                autoWidth: true
            }
        }

    });

    $('.owl-carousel-ladder-news').owlCarousel({
        loop:true,
        margin: 0,
        navText: ['',''],
        nav: true,

        responsive:{
            0:{
                items: 4,
                autoWidth: true
            }
        }

    });



    $(document).on('mouseenter', '.header-menu li', function (e)
    {
        let $this = $(this);
        if($this.find('.menu-additional-container').length) {
            $this.find('.menu-additional-container').fadeIn(200);
            e.preventDefault();
        }
    });

    $(document).on('mouseleave', '.header-menu li', function (e)
    {
        let $this = $(this);
        if($this.find('.menu-additional-container').length) {
            $this.find('.menu-additional-container').hide();
            e.preventDefault();
        }
    });


    $(document).on('mouseover', '.block-search .btn-search', function (e)
    {
        let $this = $(this);
        if ($this.closest('.search-block-cont').find('.input-search').is(":visible")) {
            return true;
        }
        else {
            e.preventDefault();
            $this.closest('.search-block-cont').addClass('active');
            $this.closest('.search-block-cont').find('.input-search').animate({width: 'toggle'}, 240);
            $this.closest('.search-block-cont').find('.input-search').focus();
        }
    });


    $(document).click(function(event) {
        if ($(event.target).closest(".block-search").length) return;
        $('.block-search .search-block-cont').removeClass('active');
        $('.block-search .search-block-cont').find('.input-search').hide();
        event.stopPropagation();
    });

    $(document).on('mouseover', '.container-home-galery-wrapper .galery-slide-block', function (e)
    {
        let $this = $(this);
        $this.find('.gray-galery').remove();
        $this.find('.img-galery').removeClass('filter');
        $this.css({
            'position':'absolute'
        });

    });

    $(document).on('mouseout', '.container-home-galery-wrapper .galery-slide-block', function (e)
    {
        let $this = $(this);
        $this.find('.img-galery').addClass('filter');
        $this.append('<div class="gray-galery"></div>');
    });


    $(document).on('mouseenter', '.container-home-galery-wrapper .galery-slide-block.galery-first', function (e)
    {
        let $this = $(this);
        $this.css({
             'z-index': '10'
        });
        $this.animate({width: '100%'}, 250);
    });

    $(document).on('mouseleave', '.container-home-galery-wrapper .galery-slide-block.galery-first', function (e)
    {
        let $this = $(this);
        $this.animate({width: '33.33%'},250, "linear", function(){
            $this.css({
                'z-index': '5'
            });
        });
    });


    $(document).on('mouseenter', '.container-home-galery-wrapper .galery-slide-block.galery-second', function (e)
    {
        let $this = $(this);
        $this.css({
            'z-index': '10'
        });
        $this.animate({left: '0px', width: '100%'}, 250);
        $this.find('.img-galery').animate({left: '0px'}, 250);

    });

    $(document).on('mouseleave', '.container-home-galery-wrapper .galery-slide-block.galery-second', function (e)
    {
        let $this = $(this);
        $this.animate({left: '33.33%', width: '33.33%'},250, "linear", function(){
            $this.css({
                'z-index': '5'
            });
        });
        $this.find('.img-galery').animate({left: '-33.33%'}, 250);
    });




    $(document).on('mouseenter', '.container-home-galery-wrapper .galery-slide-block.galery-third', function (e)
    {
        let $this = $(this);
        $this.css({
            'z-index': '10'
        });
        $this.animate({width: '100%'}, 250);
    });

    $(document).on('mouseleave', '.container-home-galery-wrapper .galery-slide-block.galery-third', function (e)
    {
        let $this = $(this);
        $this.animate({width: '33.33%'},250, "linear", function(){
            $this.css({
                'z-index': '5'
            });
        });
    });




    $(function(){
        $('.select2-filter').select2({
            minimumResultsForSearch: Infinity
        });
    });


    $(document).on('mouseover', '.button-add-cart-red-block', function (e)
    {
        let $this = $(this);
        let $contCart = $this.closest('.container-add-cart');
        $contCart.find('h3').addClass('hover');
        $contCart.find('.decor').addClass('hover');
        $contCart.addClass('white');
    });
    $(document).on('mouseout', '.button-add-cart-red-block', function (e)
    {
        let $this = $(this);
        let $contCart = $this.closest('.container-add-cart');
        $contCart.find('h3').removeClass('hover');
        $contCart.find('.decor').removeClass('hover');
        $contCart.removeClass('white');
    });




        $('.owl-carousel-product-page').owlCarousel({
            thumbs: true,
            thumbsPrerendered: true,
            loop:false,
            margin: 0,
            nav: false,
            items: 1,
        });



        $( '#elastislide-vertical' ).elastislide( {
            orientation : 'horizontal',
            minItems : 4
        } );


    $( '#elastic' ).elastislide();




    $('.owl-carousel-chinese-dance-testimonials').owlCarousel({
        loop:true,
        margin: 0,
        navText: ['',''],
        nav: true,
        responsive:{
            0:{
                items: 4,
                autoWidth: true
            }
        }
    });


    $(document).on('click', '.owl-carousel-chinese-dance-testimonials .item', function (e)
    {
        let $this = $(this);
        let $contParent = $(this).closest('.owl-carousel-chinese-dance-testimonials');
        $contParent.find('.item').removeClass('active');
        $(this).addClass('active');
        let $preBlock = $('.pre-cont-carousel-chinese-dance-testimonials');
        $preBlock.find('.pre-name').html($(this).attr('data-pre-name'));
        $preBlock.find('.pre-position').html($(this).attr('data-pre-position'));
        $preBlock.find('.pre-image').attr('src',$(this).attr('data-pre-image'));
    });



});
